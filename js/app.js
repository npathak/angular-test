'use strict';
var app = angular.module('feedbackApp', ['ngRoute']);

app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
    $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
    $routeProvider.when('/',{templateUrl: 'partials/feedback.html', controller: 'feedbackController'});
    $routeProvider.when('/login',{templateUrl: 'partials/login.html', controller: 'loginController'});
    $routeProvider.when('/admin',{templateUrl: 'partials/admin.html', controller: 'adminController'});
    $routeProvider.otherwise({redirectTo: '/'});
}]);

app.run(function ($rootScope, $location, loginService) {
    var routePermission = ['/login'];
    $rootScope.$on('$routeChangeStart', function(){
        if(routePermission.indexOf($location.path()) !=-1){
            var connected = loginService.isLoggedIn();
            connected.then(function(msg){
               if(!msg.data) $location.path('/admin');
            });
        }
    })
});

app.controller('feedbackController', ['$scope', '$http', function($scope, $http){
    $scope.feedback = {};
    $scope.submitFeedbackForm = function(){
        $http({
            method: 'POST',
            url: 'data/submitFeedback.php',
            data: $scope.feedback
       })
            .success(function(data){

                $scope.message = data.message;
        })
            .error(function(data){
                $scope.message = data.message;
            })
        ;
    }
    }]);

app.controller('loginController', ['$scope', '$http', '$location', function($scope, $http, $location){
    $scope.credentials = {};
    $scope.submitCredentials = function(){
        $http({
            method: 'POST',
            url: 'data/user.php',
            data: $scope.credentials
        })
            .success(function(data){
                if(!data.success) {
                   $scope.message = data.message;
               }
               else{
                    $.location.path('/login')
               }
            });
    }
}]);


app.controller('adminController', function ($scope, $http, $location, loginService) {
    if(loginService.isLoggedIn()){
        $http({
            method: 'GET',
            url: 'data/retrieveFeedback.php'
        })
            .success(function(data){
                $scope.feedbackData = data;
            });
    }
    else {
        $location.path('login')
    }



});