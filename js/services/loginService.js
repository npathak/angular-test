'use stict';
app.factory('loginService', function($http, $location, sessionService){
   return {
       login:function (data, scope) {
           var $promise = $http.post('data/user.php', data);
           $promise.then(function(msg){
               var uid = msg.data;
               if(uid){
                   sessionService.set('uid', uid);
                   $location.path('/admin');
               }
               else{
                    scope.msgtxt = 'Invalid Credentials'
                   $location.path('/');
               }
           });
       },
       logout:function() {
           sessionService.destroy('uid');
           $location.path('/');
       },
       isLoggedIn:function() {
           var $checkSessionData= $http.post('data/check_session.php');
           return $checkSessionData;
       }
   }
});