# Installation

  - Requires MySQL Database and PHP server
  - Requires mysql_pdo extension
  - Create Database and import schema from ```data/feedback.sql```
  - Copy ```data/database_connection-example.php``` to ```database_connection.php``` and replace default values with your database credentials. 
  - Copy files to webroot
  - Launch index.html

# TODO
  - Create REST API instead of using php processing scripts
  - Add ability to destroy sessions (currently, once logged in, session stays until cache cleared)
  - Create Multiple Image upload
  - "Logout/Back" button
  - Seperate controllers into diffent files

# Notes
- When accessing 'Admin' panel, username is ```admin``` and password is ```admin```