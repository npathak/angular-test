<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('database_connection.php');

$postData = json_decode(file_get_contents('php://input'));

$errors = array();  // array to hold validation errors
$data = array();        // array to pass back data

// Validate
if(!$postData->email){
    $errors['email'] = 'Email is required.';
}

if(!$postData->description){
    $errors['description'] = 'Description is required.';

}

// response if there are validation errors
if ( ! empty($errors)) {

    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
}
else {


    $email = $postData->email;
    $description = $postData->description;

    $formData = array(':email' => $email, ':description' => $description);


    $query = "INSERT INTO feedback(email, description) VALUES (:email, :description)";
    $statement = $connect->prepare($query);
    if($statement->execute($formData)){
        $data['success'] = true;
        $data['message'] = 'Your feedback has been submitted!';
    }
    else{

        $data['success'] = false;
        $data['message'] = 'Something went wrong. Please try again.';
    }

}
 echo json_encode($data);